
var server = "http://2.msr-mbnr.appspot.com/api";
// var server = "http://localhost:3010/itdept/api/";
//var server = "/api"


export const URL = {
    HOME: server + "/home_web/",
    GALLERY: server + "/gallery_web/",
    NEWS: server + "/news/",
    LEADERS: server + "/leaders/",
    EVENTS: server + "/events/",
    ISSUES: server + "/issues/",
    ABOUTUS: server + "/about_us/",
    CONFIG: server + "/config/",
    DASHBOARD: server + "/dboard/",
    CONTACTUS: server + "/contact_us/"

}

/* export const URL = {
    LOGIN : server + "login",
    GETBOOKEDROOMS : server + "getRoomsByDates",
    BOOKINGROOMS:server + "Booking",
     GET_BOOKING: server +"getBooking",
     ROOM_CANCEL:server+"cancelroom",
     CHECKIN_INFO:server+"checkin",
     CHECKOUT_INFO:server+"checkout",
     GET_CHECKIN_INFO:server+"getcheckindata"
    
} */