import { Observable } from "rxjs/Rx"
import { Injectable } from "@angular/core"
import { Http, Response } from "@angular/http"

import 'rxjs/add/operator/map'

@Injectable()
export class ServiceProvider {

    constructor(private http: Http) { }

    fetch(url: string): Observable<any> {
        return this.http.get(url)
            .map(res => {
                let data = res.json()
                return data;
            })
    }



}
