import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../Providers/service';
import 'rxjs'
import { URL } from '../../model/url'
import { Observable } from 'rxjs/Observable';
/**
 * Generated class for the CurrentproblemsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-currentproblems',
  templateUrl: 'currentproblems.html',
})
export class CurrentproblemsPage {

  
  link: string;
  constructor(public service: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
   this.link ='http://s1msrmbnr.magnyfied.com/img/'
  }


  users: any;

  ionViewDidEnter() {
    this.service.fetch(URL.ISSUES)
      .subscribe((x) => {
        this.users = x.i;
        console.log(this.users);
      })

      
  }

}
