import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from "../../Providers/service"
import 'rxjs'
import { URL } from '../../model/url'
import { Observable } from 'rxjs/Observable';


@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {

  link: string;
  constructor(public service: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.link = 'http://s1msrmbnr.magnyfied.com/img/'
  }


  users: any;

  ionViewDidEnter() {
    this.service.fetch(URL.NEWS)
      .subscribe((x) => {
        this.users = x.n;
        console.log(this.users);
      })
  }
  news(item){
    this.navCtrl.push('NewsdetailsPage',{'evt1':item})
  }
}




