import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import 'rxjs/Rx' ;




declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-gallaryimages',
  templateUrl: 'gallaryimages.html',
})
export class GallaryimagesPage {

  http: any;
  downloadFile(arg0: any): any {
    throw new Error("Method not implemented.");
  }
  _reportService: any;
  FileTransfer: any;
  alertCtrl: any;
  open;
  link: string;
  users: any;
  photo: any;
  storageDirectory: string = '';
  platform: any;

  urls: any;
  albums: string;


  constructor(public navCtrl: NavController, public navParams: NavParams, private file: File, private socialSharing: SocialSharing, public photoLibrary: PhotoLibrary, private transfer: FileTransfer) {
    this.link = 'http://s1msrmbnr.magnyfied.com/img/'
    this.open = this.navParams.get('selectedImage')
    console.log("open", this.open)
  }

  /*  download(item) {
    console.log(this.urls);
    this.photoLibrary.saveImage(`${this.link}${item.surl}`, this.albums)
      .then(data => {
        console.log(this.urls);
      })
      .catch(error => {

        console.log(error);

      });
  }  */

  /* download(item) {

    const fileTransfer: FileTransferObject = this.transfer.create();
    // const imageLocation = `${cordova.file.applicationDirectory}www/assets/img/${image}`;
    const url = encodeURI(`${this.link}${item.surl}`);

    fileTransfer.download(url, this.file.dataDirectory + `${item.surl}.png`, true).then((entry) => {
      console.log('success');
    }, (error) => {
      console.log('error');
    });

  } */



  share(item) {
    this.socialSharing.share('', '', `${this.link}${item.surl}`, '')
      .then(data => {
        console.log(this.urls);
      }).catch(() => {

      })
  }
  
  /* download(item){
    var blob = new Blob([`${this.link}${item.surl}`], );
    var url= window.URL.createObjectURL(blob);
    this.photoLibrary.saveImage(url, this.albums);
  } */


//   download() {
//     const url = '${this.link}${item.surl}';
//     FileTransfer.download(url, this.file.dataDirectory + 'file.pdf').then((entry) => {
//       console.log('download complete: ' + entry.toURL());
//     }
 }
