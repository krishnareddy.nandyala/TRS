import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../Providers/service';
import 'rxjs'
import { URL } from '../../model/url'
import { Observable } from 'rxjs/Observable';
/**
 * Generated class for the LocalleadersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-localleaders',
  templateUrl: 'localleaders.html',
})
export class LocalleadersPage {

  villages: any;
  link: string;
  constructor(public service: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.link = 'http://s1msrmbnr.magnyfied.com/img/'
  }

  item: any;
  users: any;
  village: any;
  ward: any;
  repos: any;

  ionViewDidEnter() {
    this.service.fetch(URL.LEADERS)
      .subscribe((x) => {
        this.users = x.l;
        console.log(this.users);
      })


  }

  selectTown(): void {

    this.villages = this.users.filter(item => item.m === this.ward);
  }

  selectWard() {
    this.repos = this.villages.filter(item => item.v === this.village);;
  }
}
