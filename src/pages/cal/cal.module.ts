import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalPage } from './cal';

import { CalendarModule } from 'ionic3-calendar-en';

@NgModule({
  declarations: [
    CalPage,
  ],
  imports: [
    CalendarModule,
    IonicPageModule.forChild(CalPage),
  ],
})
export class CalPageModule { }
