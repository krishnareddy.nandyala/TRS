import { Component } from '@angular/core';
import { NavController, AlertController, IonicPage, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../Providers/service';
import { URL } from '../../model/url';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-cal',
  templateUrl: 'cal.html'
})
export class CalPage {


  eventdetails=[];
  link: string;
  
  users: any;
  eventData=[];
  dateevent: any;
  // Date(arg0: any): any {
  //   throw new Error("Method not implemented.");
  // }

  constructor(private alertCtrl: AlertController, public navParams: NavParams, public navCtrl: NavController, private service: ServiceProvider) {

  }
  date: string;
  type: 'string';
  
   onChangeDate(selectedDate) {
     if(selectedDate.hasEvent)
     {
    let evt= this.eventData.filter(x=>{
       return moment(x.dt).format('yyyy-dd-MM') == moment(selectedDate.year+'-'+(selectedDate.month+1)+'-'+selectedDate.date).format('yyyy-dd-MM');
     })
      this.navCtrl.push('EventdetailsPage', { 'evt': evt });

     }
     else
     {
       alert("No event on selected date")
     }
     /*  console.log(this.eventdetails) */
  } 

  //return isSame;
  //return moment(x.dt)== 
currentEvents=[];
  ionViewDidEnter() {
    this.service.fetch(URL.EVENTS)
      .subscribe((x) => {
        this.eventData = x.e;
        this.currentEvents = this.eventData.map(x=>{
         return  {
            year: moment(x.dt).year(),
            month: moment(x.dt).month(),
            date:  moment(x.dt).date(),
          }
        })
       
      })
  }

  


}
