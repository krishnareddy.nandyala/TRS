import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../Providers/service';
import 'rxjs'
import { URL } from '../../model/url'
import { Observable } from 'rxjs/Observable';
import { SocialSharing } from '@ionic-native/social-sharing';
import { PhotoLibrary } from '@ionic-native/photo-library';


@IonicPage()
@Component({
  selector: 'page-gallery',
  templateUrl: 'gallery.html',
})
export class GalleryPage {
  
  users: any;
  photo: any;

  platform: any;
  link: string;
  urls: any;
  albums: string;

  constructor(public service: ServiceProvider, public navCtrl: NavController,
    public navParams: NavParams, private socialSharing: SocialSharing, public photoLibrary: PhotoLibrary) {
    this.link = 'http://s1msrmbnr.magnyfied.com/img/'

    

  }


  /* share(item) {
    this.socialSharing.share('', '', 'http://s1msrmbnr.magnyfied.com/img/569852_1521027938.png', '')
      .then(data => {
        console.log(this.urls);
      }).catch(() => {

      })
  }
 */

share(item) {
  this.socialSharing.share('', '', `${this.link}${item.surl}`, '')
    .then(data => {
      console.log(this.urls);
    }).catch(() => {

    })
} 

  ionViewDidEnter() {
    this.service.fetch(URL.GALLERY)
      .subscribe((x) => {
        this.users = x.albums;
        console.log(this.users);
      })
  }

  /* download(item) {
    console.log(this.urls);
    this.photoLibrary.saveImage(`${this.link}${item.surl}`, this.albums)
      .then(data => {
        console.log(this.urls);
      })
      .catch(error => {

        console.log(error);

      });
  } */

  selectedImage(item,i)
  {

    this.navCtrl.push('GallaryimagesPage',{'selectedImage':{i}})
    
    console.log("selectedImage:",item)
   console.log("itemData:",i)
    
  }

}