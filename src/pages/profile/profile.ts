import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../Providers/service';
import 'rxjs'
import { URL } from '../../model/url'
import { Observable } from 'rxjs/Observable';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  link: string;
  constructor(public service: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
   this.link ='http://s1msrmbnr.magnyfied.com/img/'
  }


  users: any;

  ionViewDidEnter() {
    this.service.fetch(URL.ABOUTUS)
      .subscribe((x) => {
        this.users = x;
        console.log(this.users);
      })

      
  }

}