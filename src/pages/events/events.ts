// import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { ServiceProvider } from '../../Providers/service';
// import 'rxjs'
// import { URL } from '../../model/url'
// import { Observable } from 'rxjs/Observable';
// /**
//  * Generated class for the EventsPage page.
//  *
//  * See https://ionicframework.com/docs/components/#navigation for more info on
//  * Ionic pages and navigation.
//  */

// @IonicPage()
// @Component({
//   selector: 'page-events',
//   templateUrl: 'events.html',
// })
// export class EventsPage {

//   link: string;
//   constructor(public service: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
//     this.link = 'http://s1msrmbnr.magnyfied.com/img/'
//   }
//   item: any;
//   users: any;

//   userdetails = [];
//   ionViewDidEnter() {
//     this.service.fetch(URL.EVENTS)
//       .subscribe((x) => {
//         this.users = x.e;
//         console.log(this.users);
//       })
//   }
//   Events(user) {
//     this.navCtrl.push('EventdetailsPage', { 'evt': user });
//   }
// }
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../Providers/service';
import 'rxjs'
import { URL } from '../../model/url'
import { Observable } from 'rxjs/Observable';
/**
 * Generated class for the EventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-events',
  templateUrl: 'events.html',
})
export class EventsPage {

  link: string;
  constructor(public service: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
   this.link ='http://s1msrmbnr.magnyfied.com/img/'
  }


  users: any;

  ionViewDidEnter() {
    this.service.fetch(URL.EVENTS)
      .subscribe((x) => {
        this.users = x.e;
        console.log(this.users);
      })
  }
  Events(evt) {
    this.navCtrl.push('EventdetailsPage', { 'evt': [evt] });
       
      }
}
